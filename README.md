[![pipeline status](https://gitlab.libriciel.fr/outils/pades-valid/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/outils/pades-valid/commits/master)
[![coverage report](https://gitlab.libriciel.fr/outils/pades-valid/badges/master/coverage.svg)](https://gitlab.libriciel.fr/outils/pades-valid/commits/master)

# PAdES-valid

Service de vérification de signature PAdES.

Utilisation de la librairie iText pour récupération et validation
des signatures.

## Packaging

*Pré-requis :*
* Java 8
* Maven 3

Lancer la commande suivante à la racine du projet :
```bash
mvn package -Dmaven.test.skip=true
```

Le jar packagé sera disponible dans `target/pades-valid-${VERSION}.jar`.

## Docker

Une image docker est construite à chaque commit.
Cette image ne possède aucune dépendance.

Il suffira de lancer l'image docker du projet `pades-valid` de la façon suivante :
```bash
docker run --name pades-valid -p 8080:8080 -d gitlab.libriciel.fr:4567/outils/pades-valid
```

L'application sera ensuite accessible sur l'adresse [http://localhost:8080/](http://localhost:8080/).

## Exemple d'utilisation

Envoi d'un fichier via curl :

```bash
curl -F "file=@fichier_pdf.pdf" -X POST http://localhost:8080/
```