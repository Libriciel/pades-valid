FROM hubdocker.libriciel.fr/eclipse-temurin:8u412-b08-jre-alpine
VOLUME /tmp
ADD target/pades-valid-*.jar app.jar
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
