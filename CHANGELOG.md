# Change Log
Tous les changements notables sur ce projet seront documentés dans ce fichier.

Ce format est basé sur [Keep a Changelog](http://keepachangelog.com/)
et ce projet adhère au [Semantic Versioning](http://semver.org/).


## [1.4.10] - 2024-28-05

## Améliorations

- Mises à jour des principales bibliothèques logicielles (springboot, bouncycastle, itext)

## [1.4.9] - 2023-12-09

## Ajouts

- Prise en compte des signatures invalides dans les données renvoyées par l'API

## [1.4.8] - 2020-30-11

## Améliorations

- Ajout d'un swagger 

## [1.4.7] - 2020-18-11

## Corrections

- Certains PDF avec une version invalide dans le dictionaire "Catalog" retournaient une erreur 500

## [1.4.6] - 2020-22-06

## Corrections

- Certains PDF avec un dictionnaire invalides retournaient une erreur 500

## [1.4.5] - 2020-31-03

## Améliorations

- Déploiement automatique sur hub.docker.com

## [1.4.4] - 2019-24-10

## Corrections

- Les PDF syntaxiquement incorrects ne doivent pas retourner d'erreur 500

## [1.4.3] - 2019-19-08

## Améliorations

- Récupération d'erreurs automatique via l'outil Sentry

## Corrections

- Mauvaise méthode de déploiement

## [1.3.2] - 2019-29-05

## Corrections

- Certains formulaires posaient problème lors de leur vérification

## [1.3.1] - 2019-22-05

### Corrections

- Certains documents avec formulaire renvoyaient une erreur 500
- Les documents ayant un format de signature PKCS7 invalide renvoyaient une erreur 500

## [1.3.0] - 2019-11-02

### Améliorations

- Mise à jour de la librairie itext en version 7

## [1.2.0] - 2019-14-01

### Améliorations

- Réorganisation du code
- Mise à jour des librairies (Spring, BouncyCastle, itext)

### Corrections

- Les documents PDF avec preuve de création étaient détectés comme signés à tord
- Les documents PDF sans formulaires renvoyaient une erreur 500

## [1.1.1] - 2018-01-10

### Ajouts

- Message d'erreur plus parlant avec référence au fichier vérifié

## [1.1.0] - 2017-11-09

### Ajouts

- Changelog
- Nouvelle propriété `signatureCoversWholeDocument` pour indiquer une modification du PDF depuis la signature
