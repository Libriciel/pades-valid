#!/bin/bash

pushd src/test/resources/

for file in $(ls | grep ".gpg"); do
    gpg --passphrase ${GPGKEY} ${file}
done

popd