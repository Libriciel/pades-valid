package coop.libriciel.padesvalid.web;

import coop.libriciel.padesvalid.utils.TestUtils;
import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.nio.file.Files;
import java.util.Locale;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Créé par lhameury le 9/13/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class VerificationControllerTest {

    @LocalServerPort
    int port;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private MessageSource messageSource;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void whenNoFile_thenStatus400() throws Exception {
        mvc.perform(multipart("/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenFileNoPDF_thenStatus422ExplicitErrorMessage() {
        String filename = "filename.txt";

        String expectedMessage = messageSource.getMessage(
                "validation.error.nopdf",
                new Object[]{filename, "PDF header not found."},
                Locale.getDefault());

        given().multiPart("file", "filename.txt", "some text".getBytes()).
                post("/").
                then().statusCode(422).and().body(containsString(expectedMessage));

    }

    @Test
    public void whenFilePDF_thenStatus200() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "filename.pdf", "application/pdf", TestUtils.minimalPdf.getBytes());

        mvc.perform(multipart("/").file(file))
                .andExpect(status().isOk());
    }

    @Test
    public void whenSignedFilePDF_thenStatus200() throws Exception {
        File minimalSignedPDF
                = new File("src/test/resources/minimal-signed.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        mvc.perform(multipart("/").file(file))
                .andExpect(status().isOk());
    }

}
