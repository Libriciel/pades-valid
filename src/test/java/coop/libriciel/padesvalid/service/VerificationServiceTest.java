package coop.libriciel.padesvalid.service;

import coop.libriciel.padesvalid.error.RestInvalidPdfException;
import coop.libriciel.padesvalid.model.VerificationResult;
import coop.libriciel.padesvalid.utils.TestUtils;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Créé par lhameury le 14/01/19.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class VerificationServiceTest {

    @Autowired
    private VerificationService verificationService;

    @Test
    public void notSigned_shouldReturnEmptyCollection() throws IOException, RestInvalidPdfException {
        MockMultipartFile file = new MockMultipartFile("file", "filename.pdf", "application/pdf", TestUtils.minimalPdf.getBytes());

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }

    @Test
    public void signed_shouldReturnSignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/minimal-signed.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(1, result.getSignatures().size());
        assertTrue(result.isSigned());
    }

    @Test
    public void signedTS_shouldReturnSignatureWithTS() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/signed-timestamped.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(2, result.getSignatures().size());
        assertTrue(result.getSignatures().get(0).isTimeStampValid());
        assertTrue(result.isSigned());
    }

    @Test
    public void signedCAdES_shouldReturnSignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/signed_cades.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(1, result.getSignatures().size());
        assertTrue(result.isSigned());
    }

    @Test
    public void signedOnCreation_shouldReturnSignatureEmpty() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/signature_document_creation.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(1, result.getSignatures().size());
        assertTrue(result.isSigned());
    }

    @Test
    public void invalidForm_shouldReturnSignatureEmpty() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/invalid_form.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }

    @Test
    public void invalidPdfVersion_shouldReturnSignatureEmpty() throws Exception {
        File minimalSignedButInvalidPDF
                = new File("src/test/resources/minimal-signed-invalid.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedButInvalidPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }


    @Test
    public void badForm_shouldReturnSignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/bad_form.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(1, result.getSignatures().size());
        assertTrue(result.isSigned());
    }

    @Test
    public void invalidPkcs7Signature_shouldReturnEmptySignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/invalid_pkcs7.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(1, result.getSignatures().size());
        assertTrue(result.isSigned());
        assertFalse(result.getSignatures().get(0).isValid());
    }

    @Test
    public void invalidPkcs7Signature_shouldReturnOnlyOneSignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/invalid_pkcs7_2.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(3, result.getSignatures().size());
        assertTrue(result.isSigned());
        assertFalse(result.getSignatures().get(0).isValid());
        assertFalse(result.getSignatures().get(1).isValid());
        assertTrue(result.getSignatures().get(2).isValid());
    }

    @Test
    public void invalidPdfReference_shouldReturnEmptySignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/invalid_reference.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }

    @Test
    public void syntaxErrorPdf_shouldReturnEmptySignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/syntax_error.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }

    @Test
    public void badDictionnaryPdf_shouldReturnEmptySignature() throws IOException, RestInvalidPdfException {
        File minimalSignedPDF
                = new File("src/test/resources/bad_dictionnary.pdf");

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(minimalSignedPDF.toPath()));

        VerificationResult result = verificationService.verify(file);

        assertEquals(0, result.getSignatures().size());
        assertFalse(result.isSigned());
    }
}
