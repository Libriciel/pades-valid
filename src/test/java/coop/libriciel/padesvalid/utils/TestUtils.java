package coop.libriciel.padesvalid.utils;

/**
 * Créé par lhameury le 14/01/19.
 */
public class TestUtils {

    public static String minimalPdf = "%PDF-1.2 \n" +
        "9 0 obj\n" +
        "<<\n" +
        ">>\n" +
        "stream\n" +
        "BT/ 9 Tf(Test)' ET\n" +
        "endstream\n" +
        "endobj\n" +
        "4 0 obj\n" +
        "<<\n" +
        "/Type /Page\n" +
        "/Parent 5 0 R\n" +
        "/Contents 9 0 R\n" +
        ">>\n" +
        "endobj\n" +
        "5 0 obj\n" +
        "<<\n" +
        "/Kids [4 0 R ]\n" +
        "/Count 1\n" +
        "/Type /Pages\n" +
        "/MediaBox [ 0 0 99 9 ]\n" +
        ">>\n" +
        "endobj\n" +
        "3 0 obj\n" +
        "<<\n" +
        "/Pages 5 0 R\n" +
        "/Type /Catalog\n" +
        ">>\n" +
        "endobj\n" +
        "trailer\n" +
        "<<\n" +
        "/Root 3 0 R\n" +
        ">>\n" +
        "%%EOF";

}
