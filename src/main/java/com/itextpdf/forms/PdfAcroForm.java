//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.itextpdf.forms;

import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.forms.xfa.XfaForm;
import com.itextpdf.kernel.PdfException;
import com.itextpdf.kernel.geom.AffineTransform;
import com.itextpdf.kernel.geom.Point;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfArray;
import com.itextpdf.kernel.pdf.PdfBoolean;
import com.itextpdf.kernel.pdf.PdfDictionary;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfNumber;
import com.itextpdf.kernel.pdf.PdfObject;
import com.itextpdf.kernel.pdf.PdfObjectWrapper;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfStream;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfVersion;
import com.itextpdf.kernel.pdf.VersionConforming;
import com.itextpdf.kernel.pdf.annot.PdfAnnotation;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.tagutils.TagReference;
import com.itextpdf.kernel.pdf.tagutils.TagTreePointer;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PdfAcroForm extends PdfObjectWrapper<PdfDictionary> {
    public static final int SIGNATURE_EXIST = 1;
    public static final int APPEND_ONLY = 2;
    protected boolean generateAppearance;
    protected Map<String, PdfFormField> fields;
    protected PdfDocument document;
    private static PdfName[] resourceNames;
    private PdfDictionary defaultResources;
    private Set<PdfFormField> fieldsForFlattening;
    private XfaForm xfaForm;

    private PdfAcroForm(PdfDictionary pdfObject, PdfDocument pdfDocument) {
        super(pdfObject);
        this.generateAppearance = true;
        this.fields = new LinkedHashMap();
        this.fieldsForFlattening = new LinkedHashSet();
        this.document = pdfDocument;
        this.getFormFields();
        this.xfaForm = new XfaForm(pdfObject);
    }

    private PdfAcroForm(PdfArray fields) {
        this(createAcroFormDictionaryByFields(fields), (PdfDocument)null);
        this.setForbidRelease();
    }

    public static PdfAcroForm getAcroForm(PdfDocument document, boolean createIfNotExist) {
        PdfDictionary acroFormDictionary = ((PdfDictionary)document.getCatalog().getPdfObject()).getAsDictionary(PdfName.AcroForm);
        PdfAcroForm acroForm = null;
        if (acroFormDictionary == null) {
            if (createIfNotExist) {
                acroForm = new PdfAcroForm(new PdfArray());
                acroForm.makeIndirect(document);
                document.getCatalog().put(PdfName.AcroForm, acroForm.getPdfObject());
                document.getCatalog().setModified();
            }
        } else {
            acroForm = new PdfAcroForm(acroFormDictionary, document);
        }

        if (acroForm != null) {
            acroForm.defaultResources = acroForm.getDefaultResources();
            if (acroForm.defaultResources == null) {
                acroForm.defaultResources = new PdfDictionary();
            }

            acroForm.document = document;
            acroForm.xfaForm = new XfaForm(document);
        }

        return acroForm;
    }

    public void addField(PdfFormField field) {
        if (this.document.getNumberOfPages() == 0) {
            this.document.addNewPage();
        }

        PdfPage page = this.document.getLastPage();
        this.addField(field, page);
    }

    public void addField(PdfFormField field, PdfPage page) {
        PdfArray kids = field.getKids();
        PdfDictionary fieldDic = (PdfDictionary)field.getPdfObject();
        if (kids != null) {
            this.processKids(kids, fieldDic, page);
        }

        this.getFields().add(fieldDic);
        this.fields.put(field.getFieldName().toUnicodeString(), field);
        if (field.getKids() != null) {
            this.iterateFields(field.getKids(), this.fields);
        }

        if (field.getFormType() != null && (field.getFormType().equals(PdfName.Tx) || field.getFormType().equals(PdfName.Ch))) {
            List<PdfDictionary> resources = this.getResources((PdfDictionary)field.getPdfObject());
            Iterator var6 = resources.iterator();

            while(var6.hasNext()) {
                PdfDictionary resDict = (PdfDictionary)var6.next();
                this.mergeResources(this.defaultResources, resDict);
            }

            if (!this.defaultResources.isEmpty()) {
                this.put(PdfName.DR, this.defaultResources);
            }
        }

        if (fieldDic.containsKey(PdfName.Subtype) && page != null) {
            PdfAnnotation annot = PdfAnnotation.makeAnnotation(fieldDic);
            this.addWidgetAnnotationToPage(page, annot);
        }

    }

    public void addFieldAppearanceToPage(PdfFormField field, PdfPage page) {
        PdfDictionary fieldDict = (PdfDictionary)field.getPdfObject();
        PdfArray kids = field.getKids();
        if (kids != null && kids.size() <= 1) {
            PdfDictionary kidDict = (PdfDictionary)kids.get(0);
            PdfName type = kidDict.getAsName(PdfName.Subtype);
            if (type != null && type.equals(PdfName.Widget)) {
                if (!kidDict.containsKey(PdfName.FT)) {
                    this.mergeWidgetWithParentField(fieldDict, kidDict);
                }

                this.defineWidgetPageAndAddToIt(page, fieldDict, false);
            }

        }
    }

    public Map<String, PdfFormField> getFormFields() {
        if (this.fields.size() == 0) {
            this.fields = this.iterateFields(this.getFields());
        }

        return this.fields;
    }

    public PdfDocument getPdfDocument() {
        return this.document;
    }

    public PdfAcroForm setNeedAppearances(boolean needAppearances) {
        if (VersionConforming.validatePdfVersionForDeprecatedFeatureLogError(this.document, PdfVersion.PDF_2_0, "NeedAppearances has been deprecated in PDF 2.0. Appearance streams are required in PDF 2.0.")) {
            ((PdfDictionary)this.getPdfObject()).remove(PdfName.NeedAppearances);
            return this;
        } else {
            return this.put(PdfName.NeedAppearances, PdfBoolean.valueOf(needAppearances));
        }
    }

    public PdfBoolean getNeedAppearances() {
        return ((PdfDictionary)this.getPdfObject()).getAsBoolean(PdfName.NeedAppearances);
    }

    public PdfAcroForm setSignatureFlags(int sigFlags) {
        return this.put(PdfName.SigFlags, new PdfNumber(sigFlags));
    }

    public PdfAcroForm setSignatureFlag(int sigFlag) {
        int flags = this.getSignatureFlags();
        flags |= sigFlag;
        return this.setSignatureFlags(flags);
    }

    public int getSignatureFlags() {
        PdfNumber f = ((PdfDictionary)this.getPdfObject()).getAsNumber(PdfName.SigFlags);
        return f != null ? f.intValue() : 0;
    }

    public PdfAcroForm setCalculationOrder(PdfArray calculationOrder) {
        return this.put(PdfName.CO, calculationOrder);
    }

    public PdfArray getCalculationOrder() {
        return ((PdfDictionary)this.getPdfObject()).getAsArray(PdfName.CO);
    }

    public PdfAcroForm setDefaultResources(PdfDictionary defaultResources) {
        return this.put(PdfName.DR, defaultResources);
    }

    public PdfDictionary getDefaultResources() {
        return ((PdfDictionary)this.getPdfObject()).getAsDictionary(PdfName.DR);
    }

    public PdfAcroForm setDefaultAppearance(String appearance) {
        return this.put(PdfName.DA, new PdfString(appearance));
    }

    public PdfString getDefaultAppearance() {
        return ((PdfDictionary)this.getPdfObject()).getAsString(PdfName.DA);
    }

    public PdfAcroForm setDefaultJustification(int justification) {
        return this.put(PdfName.Q, new PdfNumber(justification));
    }

    public PdfNumber getDefaultJustification() {
        return ((PdfDictionary)this.getPdfObject()).getAsNumber(PdfName.Q);
    }

    public PdfAcroForm setXFAResource(PdfStream xfaResource) {
        return this.put(PdfName.XFA, xfaResource);
    }

    public PdfAcroForm setXFAResource(PdfArray xfaResource) {
        return this.put(PdfName.XFA, xfaResource);
    }

    public PdfObject getXFAResource() {
        return ((PdfDictionary)this.getPdfObject()).get(PdfName.XFA);
    }

    public PdfFormField getField(String fieldName) {
        return (PdfFormField)this.fields.get(fieldName);
    }

    public boolean isGenerateAppearance() {
        return this.generateAppearance;
    }

    public void setGenerateAppearance(boolean generateAppearance) {
        if (generateAppearance) {
            ((PdfDictionary)this.getPdfObject()).remove(PdfName.NeedAppearances);
        }

        this.generateAppearance = generateAppearance;
    }

    public void flattenFields() {
        if (this.document.isAppendMode()) {
            throw new PdfException("Field flattening is not supported in append mode.");
        } else {
            LinkedHashSet fields;
            if (this.fieldsForFlattening.size() == 0) {
                this.fields.clear();
                fields = new LinkedHashSet(this.getFormFields().values());
            } else {
                fields = new LinkedHashSet();
                Iterator var2 = this.fieldsForFlattening.iterator();

                while(var2.hasNext()) {
                    PdfFormField field = (PdfFormField)var2.next();
                    fields.addAll(this.prepareFieldsForFlattening(field));
                }
            }

            Map<Integer, PdfObject> initialPageResourceClones = new LinkedHashMap();

            for(int i = 1; i <= this.document.getNumberOfPages(); ++i) {
                PdfObject resources = ((PdfDictionary)this.document.getPage(i).getPdfObject()).getAsDictionary(PdfName.Resources);
                initialPageResourceClones.put(i, resources == null ? null : resources.clone());
            }

            Set<PdfPage> wrappedPages = new LinkedHashSet();
            Iterator var5 = fields.iterator();

            while(true) {
                PdfFormField field;
                PdfDictionary fieldObject;
                PdfPage page;
                do {
                    if (!var5.hasNext()) {
                        ((PdfDictionary)this.getPdfObject()).remove(PdfName.NeedAppearances);
                        if (this.fieldsForFlattening.size() == 0) {
                            this.getFields().clear();
                        }

                        if (this.getFields().isEmpty()) {
                            this.document.getCatalog().remove(PdfName.AcroForm);
                        }

                        return;
                    }

                    field = (PdfFormField)var5.next();
                    fieldObject = (PdfDictionary)field.getPdfObject();
                    page = this.getFieldPage(fieldObject);
                } while(page == null);

                PdfAnnotation annotation = PdfAnnotation.makeAnnotation(fieldObject);
                TagTreePointer tagPointer = null;
                if (annotation != null && this.document.isTagged()) {
                    tagPointer = this.document.getTagStructureContext().removeAnnotationTag(annotation);
                }

                PdfDictionary appDic = fieldObject.getAsDictionary(PdfName.AP);
                PdfObject asNormal = null;
                if (appDic != null) {
                    asNormal = appDic.getAsStream(PdfName.N);
                    if (asNormal == null) {
                        asNormal = appDic.getAsDictionary(PdfName.N);
                    }
                }

                if (this.generateAppearance && (appDic == null || asNormal == null)) {
                    field.regenerateField();
                    appDic = fieldObject.getAsDictionary(PdfName.AP);
                }

                if (appDic != null) {
                    PdfObject normal = appDic.get(PdfName.N);
                    PdfFormXObject xObject = null;
                    if (normal.isStream()) {
                        xObject = new PdfFormXObject((PdfStream)normal);
                    } else if (normal.isDictionary()) {
                        PdfName as = fieldObject.getAsName(PdfName.AS);
                        if (((PdfDictionary)normal).getAsStream(as) != null) {
                            xObject = new PdfFormXObject(((PdfDictionary)normal).getAsStream(as));
                            xObject.makeIndirect(this.document);
                        }
                    }

                    if (xObject != null) {
                        xObject.put(PdfName.Subtype, PdfName.Form);
                        Rectangle annotBBox = fieldObject.getAsRectangle(PdfName.Rect);
                        if (page.isFlushed()) {
                            throw new PdfException("The page has been already flushed. Use PdfAcroForm#addFieldAppearanceToPage() method before page flushing.");
                        }

                        PdfCanvas canvas = new PdfCanvas(page, !wrappedPages.contains(page));
                        wrappedPages.add(page);
                        PdfObject xObjectResources = ((PdfStream)xObject.getPdfObject()).get(PdfName.Resources);
                        PdfObject pageResources = page.getResources().getPdfObject();
                        if (xObjectResources != null && pageResources != null && xObjectResources == pageResources) {
                            ((PdfStream)xObject.getPdfObject()).put(PdfName.Resources, (PdfObject)initialPageResourceClones.get(this.document.getPageNumber(page)));
                        }

                        if (tagPointer != null) {
                            tagPointer.setPageForTagging(page);
                            TagReference tagRef = tagPointer.getTagReference();
                            canvas.openTag(tagRef);
                        }

                        AffineTransform at = this.calcFieldAppTransformToAnnotRect(xObject, annotBBox);
                        float[] m = new float[6];
                        at.getMatrix(m);
                        canvas.addXObject(xObject, m[0], m[1], m[2], m[3], m[4], m[5]);
                        if (tagPointer != null) {
                            canvas.closeTag();
                        }
                    }
                }

                PdfArray fFields = this.getFields();
                fFields.remove(fieldObject);
                if (annotation != null) {
                    page.removeAnnotation(annotation);
                }

                PdfDictionary parent = fieldObject.getAsDictionary(PdfName.Parent);
                if (parent != null) {
                    PdfArray kids = parent.getAsArray(PdfName.Kids);
                    if (kids != null) {
                        kids.remove(fieldObject);
                        if (kids.isEmpty()) {
                            fFields.remove(parent);
                        }
                    } else {
                        fFields.remove(parent);
                    }
                }
            }
        }
    }

    public boolean removeField(String fieldName) {
        PdfFormField field = this.getField(fieldName);
        if (field == null) {
            return false;
        } else {
            PdfDictionary fieldObject = (PdfDictionary)field.getPdfObject();
            PdfPage page = this.getFieldPage(fieldObject);
            PdfAnnotation annotation = PdfAnnotation.makeAnnotation(fieldObject);
            if (page != null && annotation != null) {
                page.removeAnnotation(annotation);
            }

            PdfDictionary parent = field.getParent();
            if (parent != null) {
                parent.getAsArray(PdfName.Kids).remove(fieldObject);
                this.fields.remove(fieldName);
                return true;
            } else {
                PdfArray fieldsPdfArray = this.getFields();
                if (fieldsPdfArray.contains(fieldObject)) {
                    fieldsPdfArray.remove(fieldObject);
                    this.fields.remove(fieldName);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public void partialFormFlattening(String fieldName) {
        PdfFormField field = (PdfFormField)this.getFormFields().get(fieldName);
        if (field != null) {
            this.fieldsForFlattening.add(field);
        }

    }

    public void renameField(String oldName, String newName) {
        Map<String, PdfFormField> fields = this.getFormFields();
        if (!fields.containsKey(newName)) {
            PdfFormField field = (PdfFormField)fields.get(oldName);
            if (field != null) {
                field.setFieldName(newName);
                fields.remove(oldName);
                fields.put(newName, field);
            }

        }
    }

    public PdfFormField copyField(String name) {
        PdfFormField oldField = this.getField(name);
        if (oldField != null) {
            PdfFormField field = new PdfFormField((PdfDictionary)((PdfDictionary)oldField.getPdfObject()).clone().makeIndirect(this.document));
            return field;
        } else {
            return null;
        }
    }

    public void replaceField(String name, PdfFormField field) {
        this.removeField(name);
        this.addField(field);
    }

    protected PdfArray getFields() {
        PdfArray fields = ((PdfDictionary)this.getPdfObject()).getAsArray(PdfName.Fields);
        if (fields == null) {
            Logger logger = LoggerFactory.getLogger(PdfAcroForm.class);
            logger.warn("Required AcroForm entry /Fields does not exist in the document. Empty array /Fields will be created.");
            fields = new PdfArray();
            ((PdfDictionary)this.getPdfObject()).put(PdfName.Fields, fields);
        }

        return fields;
    }

    protected boolean isWrappedObjectMustBeIndirect() {
        return false;
    }

    private Map<String, PdfFormField> iterateFields(PdfArray array, Map<String, PdfFormField> fields) {
        int index = 1;
        Iterator var4 = array.iterator();

        while(true) {
            while(var4.hasNext()) {
                PdfObject field = (PdfObject)var4.next();
                if (field.isFlushed()) {
                    Logger logger = LoggerFactory.getLogger(PdfAcroForm.class);
                    logger.warn("A form field was flushed. There's no way to create this field in the AcroForm dictionary.");
                } else {
                    PdfFormField formField = PdfFormField.makeFormField(field, this.document);
                    PdfString fieldName = formField.getFieldName();
                    String name = null;
                    if (fieldName == null) {
                        if(formField.getParent() != null) {
                            PdfFormField parentField = PdfFormField.makeFormField(formField.getParent(), this.document);

                            while(fieldName == null) {
                                fieldName = parentField.getFieldName();
                                if (fieldName == null) {
                                    parentField = PdfFormField.makeFormField(parentField.getParent(), this.document);
                                }
                            }

                            name = fieldName.toUnicodeString() + "." + index;
                            ++index;
                        }
                    } else {
                        name = fieldName.toUnicodeString();
                    }
                    if(name != null) {
                        fields.put(name, formField);
                        if (formField.getKids() != null) {
                            this.iterateFields(formField.getKids(), fields);
                        }
                    }

                }
            }

            return fields;
        }
    }

    private Map<String, PdfFormField> iterateFields(PdfArray array) {
        return this.iterateFields(array, new LinkedHashMap());
    }

    private PdfDictionary processKids(PdfArray kids, PdfDictionary parent, PdfPage page) {
        PdfArray otherKids;
        if (kids.size() == 1) {
            PdfDictionary kidDict = (PdfDictionary)kids.get(0);
            PdfName type = kidDict.getAsName(PdfName.Subtype);
            if (type != null && type.equals(PdfName.Widget)) {
                if (!kidDict.containsKey(PdfName.FT)) {
                    this.mergeWidgetWithParentField(parent, kidDict);
                    this.defineWidgetPageAndAddToIt(page, parent, true);
                } else {
                    this.defineWidgetPageAndAddToIt(page, kidDict, true);
                }
            } else {
                otherKids = kidDict.getAsArray(PdfName.Kids);
                if (otherKids != null) {
                    this.processKids(otherKids, kidDict, page);
                }
            }
        } else {
            for(int i = 0; i < kids.size(); ++i) {
                PdfObject kid = kids.get(i);
                otherKids = ((PdfDictionary)kid).getAsArray(PdfName.Kids);
                if (otherKids != null) {
                    this.processKids(otherKids, (PdfDictionary)kid, page);
                }
            }
        }

        return parent;
    }

    private void mergeWidgetWithParentField(PdfDictionary parent, PdfDictionary widgetDict) {
        parent.remove(PdfName.Kids);
        widgetDict.remove(PdfName.Parent);
        parent.mergeDifferent(widgetDict);
    }

    private void defineWidgetPageAndAddToIt(PdfPage currentPage, PdfDictionary mergedFieldAndWidget, boolean warnIfPageFlushed) {
        PdfAnnotation annot = PdfAnnotation.makeAnnotation(mergedFieldAndWidget);
        PdfDictionary pageDic = annot.getPageObject();
        if (pageDic != null) {
            if (warnIfPageFlushed && pageDic.isFlushed()) {
                throw new PdfException("The page has been already flushed. Use PdfAcroForm#addFieldAppearanceToPage() method before page flushing.");
            }

            PdfDocument doc = pageDic.getIndirectReference().getDocument();
            PdfPage widgetPage = doc.getPage(pageDic);
            this.addWidgetAnnotationToPage(widgetPage, annot);
        } else {
            this.addWidgetAnnotationToPage(currentPage, annot);
        }

    }

    private void addWidgetAnnotationToPage(PdfPage page, PdfAnnotation annot) {
        if (!page.containsAnnotation(annot)) {
            TagTreePointer tagPointer = null;
            boolean tagged = page.getDocument().isTagged();
            if (tagged) {
                tagPointer = page.getDocument().getTagStructureContext().getAutoTaggingPointer();
                tagPointer.addTag("Form");
            }

            page.addAnnotation(annot);
            if (tagged) {
                tagPointer.moveToParent();
            }

        }
    }

    private List<PdfDictionary> getResources(PdfDictionary field) {
        List<PdfDictionary> resources = new ArrayList();
        PdfDictionary ap = field.getAsDictionary(PdfName.AP);
        Iterator var5;
        if (ap != null && !ap.isFlushed()) {
            PdfObject normal = ap.get(PdfName.N);
            if (normal != null && !normal.isFlushed()) {
                if (normal.isDictionary()) {
                    var5 = ((PdfDictionary)normal).keySet().iterator();

                    while(var5.hasNext()) {
                        PdfName key = (PdfName)var5.next();
                        PdfStream appearance = ((PdfDictionary)normal).getAsStream(key);
                        PdfDictionary resDict = appearance.getAsDictionary(PdfName.Resources);
                        if (resDict != null) {
                            resources.add(resDict);
                            break;
                        }
                    }
                } else if (normal.isStream()) {
                    PdfDictionary resDict = ((PdfStream)normal).getAsDictionary(PdfName.Resources);
                    if (resDict != null) {
                        resources.add(resDict);
                    }
                }
            }
        }

        PdfArray kids = field.getAsArray(PdfName.Kids);
        if (kids != null) {
            var5 = kids.iterator();

            while(var5.hasNext()) {
                PdfObject kid = (PdfObject)var5.next();
                resources.addAll(this.getResources((PdfDictionary)kid));
            }
        }

        return resources;
    }

    private void mergeResources(PdfDictionary result, PdfDictionary source) {
        PdfName[] var3 = resourceNames;
        int var4 = var3.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            PdfName name = var3[var5];
            PdfDictionary dic = source.isFlushed() ? null : source.getAsDictionary(name);
            PdfDictionary res = result.getAsDictionary(name);
            if (res == null) {
                res = new PdfDictionary();
            }

            if (dic != null) {
                res.mergeDifferent(dic);
                result.put(name, res);
            }
        }

    }

    public boolean hasXfaForm() {
        return this.xfaForm != null && this.xfaForm.isXfaPresent();
    }

    public XfaForm getXfaForm() {
        return this.xfaForm;
    }

    public void removeXfaForm() {
        if (this.hasXfaForm()) {
            PdfDictionary root = (PdfDictionary)this.document.getCatalog().getPdfObject();
            PdfDictionary acroform = root.getAsDictionary(PdfName.AcroForm);
            acroform.remove(PdfName.XFA);
            this.xfaForm = null;
        }

    }

    public PdfAcroForm put(PdfName key, PdfObject value) {
        ((PdfDictionary)this.getPdfObject()).put(key, value);
        return this;
    }

    public void release() {
        this.unsetForbidRelease();
        ((PdfDictionary)this.getPdfObject()).release();
        Iterator var1 = this.fields.values().iterator();

        while(var1.hasNext()) {
            PdfFormField field = (PdfFormField)var1.next();
            field.release();
        }

        this.fields = null;
    }

    private static PdfDictionary createAcroFormDictionaryByFields(PdfArray fields) {
        PdfDictionary dictionary = new PdfDictionary();
        dictionary.put(PdfName.Fields, fields);
        return dictionary;
    }

    private PdfPage getFieldPage(PdfDictionary annotDic) {
        PdfDictionary pageDic = annotDic.getAsDictionary(PdfName.P);
        if (pageDic != null) {
            return this.document.getPage(pageDic);
        } else {
            for(int i = 1; i <= this.document.getNumberOfPages(); ++i) {
                PdfPage page = this.document.getPage(i);
                if (!page.isFlushed()) {
                    PdfAnnotation annotation = PdfAnnotation.makeAnnotation(annotDic);
                    if (annotation != null && page.containsAnnotation(annotation)) {
                        return page;
                    }
                }
            }

            return null;
        }
    }

    private Set<PdfFormField> prepareFieldsForFlattening(PdfFormField field) {
        Set<PdfFormField> preparedFields = new LinkedHashSet();
        preparedFields.add(field);
        PdfArray kids = field.getKids();
        if (kids != null) {
            Iterator var4 = kids.iterator();

            while(var4.hasNext()) {
                PdfObject kid = (PdfObject)var4.next();
                PdfFormField kidField = new PdfFormField((PdfDictionary)kid);
                preparedFields.add(kidField);
                if (kidField.getKids() != null) {
                    preparedFields.addAll(this.prepareFieldsForFlattening(kidField));
                }
            }
        }

        return preparedFields;
    }

    private AffineTransform calcFieldAppTransformToAnnotRect(PdfFormXObject xObject, Rectangle annotBBox) {
        PdfArray bBox = xObject.getBBox();
        if (bBox.size() != 4) {
            bBox = new PdfArray(new Rectangle(0.0F, 0.0F));
            xObject.setBBox(bBox);
        }

        float[] xObjBBox = bBox.toFloatArray();
        PdfArray xObjMatrix = ((PdfStream)xObject.getPdfObject()).getAsArray(PdfName.Matrix);
        Rectangle transformedRect;
        if (xObjMatrix != null && xObjMatrix.size() == 6) {
            Point[] xObjRectPoints = new Point[]{new Point((double)xObjBBox[0], (double)xObjBBox[1]), new Point((double)xObjBBox[0], (double)xObjBBox[3]), new Point((double)xObjBBox[2], (double)xObjBBox[1]), new Point((double)xObjBBox[2], (double)xObjBBox[3])};
            Point[] transformedAppBoxPoints = new Point[xObjRectPoints.length];
            (new AffineTransform(xObjMatrix.toDoubleArray())).transform(xObjRectPoints, 0, transformedAppBoxPoints, 0, xObjRectPoints.length);
            float[] transformedRectArr = new float[]{3.4028235E38F, 3.4028235E38F, -3.4028235E38F, -3.4028235E38F};
            Point[] var10 = transformedAppBoxPoints;
            int var11 = transformedAppBoxPoints.length;

            for(int var12 = 0; var12 < var11; ++var12) {
                Point p = var10[var12];
                transformedRectArr[0] = (float)Math.min((double)transformedRectArr[0], p.x);
                transformedRectArr[1] = (float)Math.min((double)transformedRectArr[1], p.y);
                transformedRectArr[2] = (float)Math.max((double)transformedRectArr[2], p.x);
                transformedRectArr[3] = (float)Math.max((double)transformedRectArr[3], p.y);
            }

            transformedRect = new Rectangle(transformedRectArr[0], transformedRectArr[1], transformedRectArr[2] - transformedRectArr[0], transformedRectArr[3] - transformedRectArr[1]);
        } else {
            transformedRect = (new Rectangle(0.0F, 0.0F)).setBbox(xObjBBox[0], xObjBBox[1], xObjBBox[2], xObjBBox[3]);
        }

        AffineTransform at = AffineTransform.getTranslateInstance((double)(-transformedRect.getX()), (double)(-transformedRect.getY()));
        float scaleX = transformedRect.getWidth() == 0.0F ? 1.0F : annotBBox.getWidth() / transformedRect.getWidth();
        float scaleY = transformedRect.getHeight() == 0.0F ? 1.0F : annotBBox.getHeight() / transformedRect.getHeight();
        at.preConcatenate(AffineTransform.getScaleInstance((double)scaleX, (double)scaleY));
        at.preConcatenate(AffineTransform.getTranslateInstance((double)annotBBox.getX(), (double)annotBBox.getY()));
        return at;
    }

    static {
        resourceNames = new PdfName[]{PdfName.Font, PdfName.XObject, PdfName.ColorSpace, PdfName.Pattern};
    }
}
