package coop.libriciel.padesvalid.service;

import com.itextpdf.kernel.PdfException;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.signatures.CertificateInfo;
import com.itextpdf.signatures.PdfPKCS7;
import com.itextpdf.signatures.SignatureUtil;
import coop.libriciel.padesvalid.error.RestInvalidPdfException;
import coop.libriciel.padesvalid.model.SignatureElement;
import coop.libriciel.padesvalid.model.VerificationResult;
import coop.libriciel.padesvalid.utils.COSFilterInputStream;
import coop.libriciel.padesvalid.web.VerificationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Créé par lhameury le 11/01/19.
 */
@Service
@RequestScope
public class VerificationService {

    private final MessageSource messageSource;
    private Logger logger = LoggerFactory.getLogger(VerificationController.class);

    private MultipartFile file;
    private PdfReader reader;
    private SignatureUtil signatureUtil;

    @Autowired
    public VerificationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    private static String parseHex(String hex) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        StringBuilder hexBuffer = new StringBuilder(hex.trim());
        if (hexBuffer.length() % 2 != 0) {
            hexBuffer.append('0');
        }

        int length = hexBuffer.length();

        for (int i = 0 ; i < length ; i += 2) {
            try {
                bytes.write(Integer.parseInt(hexBuffer.substring(i, i + 2), 16));
            } catch (NumberFormatException var6) {
                throw new IOException("Invalid hex string: " + hex, var6);
            }
        }

        return new String(bytes.toByteArray());
    }

    private void prepareService() throws RestInvalidPdfException, IOException {
        // On commence par vérifier la validité du fichier envoyé
        try {
            reader = new PdfReader(file.getInputStream());
        } catch (com.itextpdf.io.IOException ex) {
            String message = messageSource.getMessage(
                    "validation.error.nopdf",
                    new Object[]{file.getOriginalFilename(), ex.getLocalizedMessage()},
                    Locale.getDefault());
            logger.error(message);
            throw new RestInvalidPdfException(message);
        }
    }

    public VerificationResult verify(MultipartFile file) throws RestInvalidPdfException, IOException {
        this.file = file;
        this.prepareService();

        return verifySignatures();
    }

    private VerificationResult verifySignatures() {
        VerificationResult result = new VerificationResult();


        List<SignatureElement> signatures;
        List<String> names = new ArrayList<>();

        try {
            signatureUtil = new SignatureUtil(new PdfDocument(reader));
            names.addAll(signatureUtil.getSignatureNames());
            result.setSuccess(true);
        } catch (PdfException | NullPointerException | com.itextpdf.io.IOException | IllegalArgumentException ex) {
            logger.error(ex.getMessage());
            result.setErrorMessage(ex.getMessage());
        }


        // On map chaque nom de signature sur une extraction
        signatures = names.stream().map(this::extractSignatureElement).filter(Objects::nonNull).collect(Collectors.toList());

        result.setSignatures(signatures);

        // Le document est signé si un champ de signature est detecté
        result.setSigned(!signatures.isEmpty());

        return result;
    }

    /**
     * Extraction d'une unique signature
     *
     * @param name Le nom du champ de signature à extraire
     * @return Un objet SignatureElement
     */
    private SignatureElement extractSignatureElement(String name) {
        SignatureElement signatureElement = new SignatureElement();

        PdfPKCS7 pkcs7;
        try {
            pkcs7 = signatureUtil.readSignatureData(name);
        } catch (RuntimeException ex) {
            logger.error("Cannot decode pkcs7 signature");
            signatureElement.setInvalidityReason("Invalid signature data structure");
            return signatureElement;
        }

        X509Certificate cert = pkcs7.getSigningCertificate();
        Calendar cal = pkcs7.getSignDate();

        PdfDictionary sigDict = signatureUtil.getSignatureDictionary(name);

        PdfName subFilter = sigDict.getAsName(PdfName.SubFilter);
        byte[] cms = getContents(sigDict);

        boolean pkcs7Verified = false;
        byte[] certEncoded = new byte[]{};
        boolean verifiedTimestampImprint = false;

        boolean hasTimeStamp = pkcs7.getTimeStampDate() != null;

        try {
            pkcs7Verified = pkcs7.verifySignatureIntegrityAndAuthenticity();
            certEncoded = cert.getEncoded();
            if (hasTimeStamp) {
                verifiedTimestampImprint = pkcs7.verifyTimestampImprint();
            }
        } catch (GeneralSecurityException ex) {
            logger.error(messageSource.getMessage("validation.error.security", new Object[]{name, ex.getLocalizedMessage()}, Locale.getDefault()));
            // Cette erreur n'est pas relancée car totalement "isolée"
        }

        if (subFilter.toString().isEmpty() || cms.length == 0) {
            logger.warn("Wrong signature with empty subfilter or cms.");
            signatureElement.setValid(false);
            signatureElement.setInvalidityReason("Signature data has been tampered with");
        } else {
            signatureElement.setValid(pkcs7Verified);
            if (!pkcs7Verified)
                signatureElement.setInvalidityReason("Signed content does not match signature");
        }

        signatureElement.setSignedBy(CertificateInfo.getSubjectFields(cert).getField("CN"));
        signatureElement.setSignatureDate(cal.getTimeInMillis());
        signatureElement.setSigningCert(certEncoded);

        if (hasTimeStamp) {
            signatureElement.setTimeStampDate(pkcs7.getTimeStampDate().getTimeInMillis());
            signatureElement.setTimeStampValid(verifiedTimestampImprint);
        }

        signatureElement.setSignatureCoversWholeDocument(signatureUtil.signatureCoversWholeDocument(name));

        return signatureElement;
    }

    private byte[] getContents(PdfDictionary sigDict) {
        PdfArray arr = sigDict.getAsArray(PdfName.ByteRange);

        int[] byteRange = new int[arr.size()];

        for (int i = 0 ; i < byteRange.length ; ++i) {
            byteRange[i] = arr.getAsNumber(i).intValue();
        }

        int begin = byteRange[0] + byteRange[1] + 1;
        int len = byteRange[2] - begin;

        byte[] contentBytes = new byte[0];
        try {
            contentBytes = getContents(new COSFilterInputStream(file.getInputStream(), new int[]{begin, len}));
        } catch (IOException e) {
            logger.warn("Signature détectée mais ignorée (invalid hex char)");
        }
        return contentBytes;
    }

    private byte[] getContents(COSFilterInputStream fis) throws IOException {
        ByteArrayOutputStream byteOS = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];

        int c;
        while ((c = fis.read(buffer)) != -1) {
            if (buffer[0] != 60 && buffer[0] != 40) {
                if (buffer[c - 1] != 62 && buffer[c - 1] != 41) {
                    byteOS.write(buffer, 0, c);
                } else {
                    byteOS.write(buffer, 0, c - 1);
                }
            } else {
                byteOS.write(buffer, 0, c);
            }
        }

        fis.close();
        return parseHex(byteOS.toString()).getBytes();
    }
}
