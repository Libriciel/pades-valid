package coop.libriciel.padesvalid.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Créé par lhameury le 9/4/17.
 */
// On en a besoin pour la serialisation JSON
@Data
public class VerificationResult {
    private boolean isSigned;
    private List<SignatureElement> signatures;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorMessage;
    private boolean success;
}
