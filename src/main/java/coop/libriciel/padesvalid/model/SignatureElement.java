package coop.libriciel.padesvalid.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Créé par lhameury le 9/11/17.
 */
@Data
public class SignatureElement {
    private boolean isValid;
    private String signedBy;
    private long signatureDate;
    private byte[] signingCert;
    private boolean timeStampValid;
    private long timeStampDate;
    private boolean signatureCoversWholeDocument;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String invalidityReason;
}
