package coop.libriciel.padesvalid.web;

import coop.libriciel.padesvalid.error.RestInvalidPdfException;
import coop.libriciel.padesvalid.model.VerificationResult;
import coop.libriciel.padesvalid.service.VerificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

/**
 * Permet la validation d'un PDF signé (ou pas) à l'aide de la librairie itext
 * <p>
 * Créé par lhameury le 9/4/17.
 */

@RestController
@ResponseBody
public class VerificationController {

    private final VerificationService verificationService;
    // Messages d'i18n (messages.properties)
    private Logger logger = LoggerFactory.getLogger(VerificationController.class);

    @Autowired
    public VerificationController(VerificationService verificationService) {
        this.verificationService = verificationService;
    }

    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE)
    public VerificationResult validate(@RequestPart(value = "file") MultipartFile file)
            throws IOException, RestInvalidPdfException {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Verify signature(s) in %s file", file.getOriginalFilename()));
        }

        return verificationService.verify(file);
    }
}
