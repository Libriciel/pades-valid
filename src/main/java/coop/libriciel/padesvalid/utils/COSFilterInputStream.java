package coop.libriciel.padesvalid.utils;

import lombok.extern.log4j.Log4j2;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Créé par lhameury le 11/01/19.
 */

@Log4j2
public class COSFilterInputStream extends FilterInputStream {
    private final int[] byteRange;
    private long position = 0L;

    public COSFilterInputStream(InputStream in, int[] byteRange) {
        super(in);
        this.byteRange = byteRange;
    }

    @Override
    public int read() throws IOException {
        this.nextAvailable();
        int i = super.read();
        if (i > -1) {
            ++this.position;
        }

        return i;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        } else {
            int c = this.read();
            if (c == -1) {
                return -1;
            } else {
                b[off] = (byte) c;
                int i = 1;

                try {
                    while (i < len) {
                        c = this.read();
                        if (c == -1) {
                            break;
                        }

                        b[off + i] = (byte) c;
                        ++i;
                    }
                } catch (IOException ex) {
                    log.debug("Exception ignored", ex);
                }

                return i;
            }
        }
    }

    private boolean inRange() {
        long pos = this.position;

        for (int i = 0; i < this.byteRange.length / 2; ++i) {
            if ((long) this.byteRange[i * 2] <= pos && (long) (this.byteRange[i * 2] + this.byteRange[i * 2 + 1]) > pos) {
                return true;
            }
        }

        return false;
    }

    private void nextAvailable() throws IOException {
        while (true) {
            if (!this.inRange()) {
                ++this.position;
                if (super.read() >= 0) {
                    continue;
                }
            }

            return;
        }
    }
}
