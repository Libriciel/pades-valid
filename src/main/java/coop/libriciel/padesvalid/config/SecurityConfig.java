package coop.libriciel.padesvalid.config;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.context.annotation.Configuration;

import java.security.Security;

/**
 * Créé par lhameury le 9/13/17.
 */
@Configuration
public class SecurityConfig {
    public SecurityConfig() {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
    }
}
