package coop.libriciel.padesvalid.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Créé par lhameury le 9/13/17.
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class RestInvalidPdfException extends Exception {
    public RestInvalidPdfException(String s) {
        super(s);
    }
}
